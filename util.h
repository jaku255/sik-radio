#ifndef RADIO_UTIL_H
#define RADIO_UTIL_H

#include <cstdint>
#include <istream>
#include <stdexcept>
#include "proto.h"
#include "proto_audio.h"

#ifdef DEBUG
#define INFO(x) (std::cerr << x << std::endl)
#define DEBUGDATA(x) x
#else
#define INFO(x)
#define DEBUGDATA(x)
#endif

class useful_error : public std::exception {
public:
	virtual const std::string text() = 0;
};

class startup_error : public useful_error {
	const char* info;
	const int err;
public:
	explicit startup_error(const char* what) noexcept : info(what), err(errno) {}
	const char *what() const noexcept override {
		return info;
	}
	
	const std::string text() override {
		if (err)
			return std::string(info) + ": " + strerror(err);
		return info;
	}
};

class run_error : public useful_error {
	const char* info;
	const int err;
public:
	explicit run_error(const char* what) noexcept : info(what), err(errno) {}
	const char *what() const noexcept override {
		return info;
	}
	
	const std::string text() override {
		if (err)
			return std::string(info) + ": " + strerror(err);
		return info;
	}
};

template<typename T, typename = typename std::enable_if<std::is_arithmetic_v<T>, T>::type>
T strtounum(const char* start, const char **numend) {
	unsigned long long val = strtoull(start, const_cast<char **>(numend), 10);
	if (start == *numend) throw std::invalid_argument("NaN");
	if (val > std::numeric_limits<T>::max()) throw std::out_of_range("");
	return static_cast<T>(val);
}

class buffer {
	std::uint64_t capacity;         ///< How many bytes back can be read
	std::uint64_t packet_size;      ///< Size of a packet
	std::uint64_t real_capacity;    ///< How big the buffer is REALLY (we want it to hold entire packets)
	packid_t next_packet_id;     ///< Id of first byte of next packet
	std::uint64_t bytes_stored;       ///< Number of bytes stored
	std::uint64_t sent_head;        ///< Position of last byte of last packet sent
	char* buf;
	
	std::uint64_t _distance(std::uint64_t snd, std::uint64_t fst) const {
		if (snd == fst) return 0;
		if (snd > fst) return snd - fst;
		return capacity - (fst - snd);
	}
public:
	
	buffer(std::uint64_t _packet_size, std::uint64_t _capacity) :
      capacity(_capacity), packet_size(_packet_size), real_capacity(_packet_size*std::max(2UL, (_capacity / _packet_size)+1)),
      next_packet_id(0), bytes_stored(0) {
		sent_head = real_capacity - 1;
		buf = new char[real_capacity];
	}
	
	bool has_next() {
		return bytes_stored >= packet_size;
	}
	
	~buffer() {
		delete[] buf;
	}
	
	
	/// Retransmit packet with id (if still possible)
	const char* packet(packid_t id) {
		if (id >= next_packet_id) {
			INFO("Requested future packet " << id << " (now "<<next_packet_id<<")");
			return nullptr;
		}
		if (id % packet_size) {
			INFO("Requested packet is not on boundary");
			return nullptr;
		}
		if (id < next_packet_id - capacity + bytes_stored) {
			INFO("Requested packet is too old");
			return nullptr;
		}
		auto dist = next_packet_id - id;
		auto rpos = (sent_head + 1 + dist) % real_capacity;
		auto pos = real_capacity - rpos;
		return buf + pos;
	}
	
	/// Get new packet if complete
	const char* packet() {
		if (has_next()) {
			
			auto rpos = (sent_head + 1) % real_capacity;
			sent_head = (sent_head + packet_size) % real_capacity;
			next_packet_id += packet_size;
			bytes_stored -= packet_size;
			
			INFO("Sending up to" << next_packet_id - 1 << ", start " << rpos << ", end " << sent_head << " (now "<<bytes_stored <<" / "<<real_capacity<<")");
			return buf + rpos;
		}
		return nullptr;
	}
	
	/// Try to complete next packet
	void load() {
		if (real_capacity - bytes_stored == 0) {
			INFO("Trying to read, but nowhere to!");
		}
		auto write_head = (sent_head + 1 + bytes_stored) % real_capacity;
		// Two possibilities (# is data):
		// [      ####   ] - we can read to end of buffer in one call
		size_t free_bytes = real_capacity - write_head;
		// [##    #######] - we can fill in the rest
		if (write_head < sent_head)
			free_bytes -= real_capacity - sent_head;
		auto fwd = read(0, buf+write_head, free_bytes);
		if (fwd > 0) {
			INFO("Read " << fwd << ", starting in "<<write_head<<" (now "<<bytes_stored + fwd<<" / "<<real_capacity<<")");
			bytes_stored += fwd;
		} else if (fwd < 0) {
			INFO("ERROR during read: "<<errno);
		}
	}
	
	packid_t next_id() {
		return next_packet_id;
	}
};

class player_buffer {
	size_t size;
	size_t read_pos; ///< Position of bext byte to read;
	char* buf;
	packid_t* ids;
	packid_t b0;
	packid_t nextid; ///< Next id to write out
	packid_t last_known_packet; ///< Highest received packet id
	bool triggered;
	bool clean;
	size_t packet_size; ///< Largest received packet size
	
	std::set<packid_t> missing() {
		std::set<packid_t> r;
		if (nextid >= last_known_packet) return r;
		// Request only missing packets <last_known_packet .. ids[read_pos]> that we can fit into buffer,
		// request whole packets and not single bytes
		auto diff = last_known_packet - nextid;
		for (size_t i = diff;
				i > (diff > size ? diff - size : 0);
				i--) {
			if (ids[(read_pos + i)%size] != ids[read_pos] + i) {
				r.insert((ids[read_pos] + i)/packet_size*packet_size);
			}
		}
		return r;
	}
public:
	void clear() {
		std::fill(ids, ids+size, (packid_t)0);
		read_pos = 0;
		triggered = false;
		clean = true;
		packet_size = 0;
		b0 = 0;
		nextid = 0;
		last_known_packet = 0;
	}
	
	player_buffer(size_t size) : size(size) {
		buf = new char[size];
		ids = new packid_t[size];
		clear();
	}
	
	std::set<packid_t> insert_data(audio::packet& packet, size_t data_size) {
		packet_size = std::max(data_size, packet_size);
		auto curr_pos = ids[read_pos];
		if (packet.first_byte_num() < curr_pos) {
			INFO("Got packet older than currently played");
			return missing();
		}
		if (clean) {
			b0 = packet.first_byte_num();
			last_known_packet = b0 + data_size - 1;
			ids[0] = b0;
			nextid = b0;
			clean = false;
			INFO("Got first packet");
			INFO("Byte 0 is now " << b0);
			INFO("Packet size is at least "<<data_size);
		}
		if (packet.first_byte_num() + data_size > b0 + size * 3 / 4) {
			triggered = true;
			//INFO("Critical size reached");
		}
		else {
			INFO("Buffer waiting: last known is " << packet.first_byte_num() - b0 << " in order, need "<<size*3/4<<" out of "<<size);
		}
		last_known_packet = std::max(last_known_packet, packet.first_byte_num() +  - 1);
		size_t first_pos = (packet.first_byte_num() - ids[read_pos] + read_pos) % size;
		DEBUGDATA(auto fps = first_pos;)
		for (size_t i = 0; i < data_size; ++i) {
			buf[first_pos] = packet.audio_data[i];
			ids[first_pos] = packet.first_byte_num() + i;
			first_pos = (first_pos + 1) % size;
		}
		INFO("Written " << data_size << " from " << fps << " to " << first_pos);
		return missing();
	}
	bool has_byte() {
		return triggered;
	}
	
	void output() {
		if (nextid != ids[read_pos]) {
			INFO("Trying to read "<< nextid << " at " << read_pos << ", found "<<ids[read_pos]);
			throw std::underflow_error("Byte not ready");
		}
		size_t len = 0;
		
		while (nextid + len == ids[read_pos + len] && read_pos + len < size && len < packet_size) {
			len++;
		}
		auto count = write(1, buf + read_pos, len);
		if (count > 0) {
			INFO("Read "<< count << "from " << read_pos << " to " << read_pos + len);
			read_pos = (read_pos + len) % size;
			nextid += len;
		}
	}
};

class retry_set {
public:
	std::vector<size_t> retries;
	std::vector<packid_t> missing;
	std::vector<boost::posix_time::ptime> offsets;
	std::string retry_str(boost::posix_time::ptime now) {
		std::string r;
		for (size_t i = 0; i < missing.size(); ++i) {
			if ((now - offsets[i]).total_seconds() > (std::make_signed_t<size_t>)retries[i]) {
				if (!r.empty()) r.append(1, ',');
				r.append(std::to_string(missing[i]));
				retries[i] += 1;
			}// else {INFO("Missing, but not requested - "<<(now - offsets[i]).total_microseconds()<<" passed");}
		}
		return r;
	}
	void update(const std::set<packid_t>& upd) {
		if (upd.empty()) return;
		
		auto itm = missing.begin();
		auto itr = retries.begin();
		auto ito = offsets.begin();
		auto itu = upd.begin();
		
		auto timer = boost::posix_time::microsec_clock::universal_time() - boost::posix_time::seconds(2);
		
		while (itu != upd.end() && itm != missing.end()) {
			if (*itm < *itu) {
				itm = missing.erase(itm);
				itr = retries.erase(itr);
				ito = offsets.erase(ito);
			} else if (*itm == *itu) {
				++itm; ++itr; ++ito;
			} else {
				itm = missing.insert(itm, *itu);
				itr = retries.insert(itr, 0);
				ito = offsets.insert(ito, timer);
			}
			++itu;
		}
		
		if (itm != missing.end()) {
			missing.erase(itm, missing.end());
			retries.erase(itr, retries.end());
			offsets.erase(ito, offsets.end());
		}
		
		while (itu != upd.end()) {
			missing.push_back(*itu);
			retries.push_back(0);
			offsets.push_back(timer);
			++itu;
		}
		if (missing.size()) INFO("Missing packets: "<<missing.size());
	}
	
	void clear() {
		missing.clear();
		retries.clear();
		offsets.clear();
	}
};

#endif //RADIO_UTIL_H
