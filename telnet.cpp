#include <iostream>
#include <memory>
#include "telnet.h"
#include "proto.h"
#include "socket.h"
#include "recv_data.h"

TelnetServer::~TelnetServer() {
	for (auto& client : clients) {
		if (client.first >= 0)
			close(client.first); // The building is on fire, but it's still polite to close the windows
	}
}

void TelnetServer::state_changed(int fd, short revents) {
	if (!revents & POLLIN) return;
	if (fd == server_fd) {
		// New client connected
		int msgsock = accept(fd, (struct sockaddr *) 0, (socklen_t *) 0);
		if (msgsock < 0)
			throw socket_error("accept");
		poll_server.observe(msgsock, this);
		clients.emplace(std::piecewise_construct,
		                std::forward_as_tuple(msgsock),
		                std::forward_as_tuple(msgsock, tuner));
	}
	else {
		// Already known client
		auto& client = clients.at(fd);
		try {
			ssize_t rval = read(fd, buf, BUF_SIZE);
			
			if (rval > 0) {
				for (ssize_t proc = 0; proc < rval; ++proc) {
					client.input((option) buf[proc]);
				}
				
				if (rval > 0) client.print();
			}
			if (rval < 0) {
				throw client_disconnected(Broken);
			} else if (rval == 0) {
				throw client_disconnected(ClientExit);
			}
		} catch (client_disconnected& e) {
			// Cleanup
			if (close(fd) < 0)
				throw socket_error("close");
			clients.erase(fd);
			poll_server.forget(fd);
		}
	}
	
}

TelnetServer::TelnetServer(PollingServer &pserver, uint16_t port) : poll_server(pserver) {
	/* Tworzymy gniazdo centrali */
	server_fd = socket(PF_INET, SOCK_STREAM, 0);
	if (server_fd < 0) {
		throw socket_error("socket");
	}
	
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);
	if (bind(server_fd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) < 0) {
		throw socket_error("bind");
	}
	
	/* Dowiedzmy się, jaki to port i obwieśćmy to światu */
	size_t length = sizeof(server);
	if (getsockname(server_fd, (struct sockaddr *) &server,
	                (socklen_t *) &length) < 0) {
		throw socket_error("getsockname");
	}
	poll_server.observe(server_fd, this);
	
	/* Zapraszamy klientów */
	if (listen(server_fd, 5) == -1) {
		throw socket_error("listen");
	}
}

void TelnetServer::menu_redraw() {
	for (auto& cl : clients) {
		cl.second.print();
	}
}

void TelnetServer::set_tuner(radio_tuner *tun) {
	tuner = tun;
}


void ClientStatus::print() {
	output << ANSI::CLS;
	output << Separator << "\r\n";
	output << "  " << Title << "\r\n";
	for (auto& ch : tuner->known_channels) {
		output << "  " << ((tuner->active_channel && ch == tuner->active_channel) ? '>' : ' ') << ' ';
		output << ch->name << "\r\n";
	}
	output << Separator;
	output << std::flush;
}

void ClientStatus::input(unsigned char b) {
	if (!filter.process(b)) return;
	if (b == ANSI::ESC) {
		seq.push_back(b);
	} else if (seq.size() > 0) {
		seq.push_back(b);
	}
	
	// Try to process ANSI escape sequence
	if (seq.size() > 1) {
		switch (seq[1]) {
		case '[':
			if (seq.size() == 3) {
				switch (seq[2]) {
					case 65: // ARROW_UP
						tuner->prev();
						break;
					case 66: // ARROW_DOWN
						tuner->next();
						break;
				} seq.clear(); break;
			}
			break;
		default:
			// It's user's problem.
			seq.clear();
		}
	}
}


bool TelnetNegotiator::process(option opt) {
	if (!iac && opt == IAC) {
		iac = true;
		history.clear();
		return false;
	}
	else if (iac) {
		if (sb) {
			option prev = NUL;
			if (!history.empty()) {
				prev = history.back();
			}
			if (prev == IAC && opt == SE) {
				// end of sb
				sb = false;
				history.pop_back();
			} else {
				// wait for full option 'string'
				history.push_back(opt);
			}
			
			if (!sb) {
				try {
					if (history.empty()) throw std::runtime_error("Empty negotiation sequence");
					// process entire option negotiation
					if (history[0] == LINEMODE) {
						if (history.size() < 2) throw std::runtime_error("Invalid LINEMODE negotiation");
						switch (history[1]) {
							case LM_MODE:
								if (history.size() < 3) throw std::runtime_error("Invalid LINEMODE MODE negotiation");
								// we want to stay in linemode 0, but can't respond to MODE_ACK
								if (!(history[2] & LM_MODE_ACK)) {
									const option force[] = {IAC, SB, LINEMODE, LM_MODE, 0, IAC, SE};
									output.write(reinterpret_cast<const char *>(force), sizeof(force));
								}
								break;
							default:
								// we don't really care
								break;
						}
					}
				}
				catch (std::runtime_error &ex) {
					INFO("WARN: " << ex.what());
				}
				reset();
			}
		} else {
			switch (cmd) {
				// No command yet
				case NUL:
					switch (opt) {
						case NOP:
							reset();
							break;
						case AYT: {
							const option ayt_resp[] = {IAC, NOP, 0};
							output << ayt_resp << std::flush;
							reset();
						}
							break;
						case WILL:
						case WONT:
						case DO:
						case DONT:
						case SB:
							cmd = opt;
							break;
						default:
							INFO("Unsupported option " << (unsigned) opt);
							reset();
					}
					break;
					// 2-part commands
				case WILL: {
					if (opt == LINEMODE || opt == SUPPRESS_GO_AHEAD) {
						// Defaults are OK.
					} else {
						// No.
						const option resp[] = {IAC, DONT, opt, 0};
						output << resp << std::flush;
					}
					reset();
				}
					break;
				case DO: {
					if (opt == SUPPRESS_GO_AHEAD) break;
					// No.
					const option resp[] = {IAC, WONT, opt, 0};
					output << resp << std::flush;
					reset();
				}
					break;
				case WONT:
				case DONT: {
					INFO("Client rejected option " << (unsigned) opt);
					reset();
					break;
				}
					// Multipart SB command
				case SB: {
					sb = true;
				}
			}
		}
		return false;
	}
	return true;
}

void TelnetNegotiator::init() {
	const option init_state[] = {IAC, DONT, LINEMODE, IAC, DO, SUPPRESS_GO_AHEAD, IAC, WILL, SUPPRESS_GO_AHEAD,
							  IAC, WILL, ECHO, 0};
	output << init_state << std::flush;
}

void TelnetNegotiator::reset() {
	iac = false;
	cmd = NUL;
	sb = false;
	history.clear();
}

void PollingServer::tick() {
	int ret = poll(observed_sockets.data(), observed_sockets.size(), timeout);
	if (ret < 0)
		throw socket_error("poll");
	
	else if (ret > 0) {
		std::vector<std::tuple<PollObserver*, int, short>> changes;
		for (auto it = observed_sockets.begin(); it != observed_sockets.end(); ++it) {
			if (it->revents & (POLLIN | POLLERR)) {
				//INFO(it->fd<<" has data");
				const auto& observer = observers.at(it->fd);
				// Handlers can change subscriptions, so we dispatch them later
				changes.emplace_back(observer, it->fd, it->revents);
			}
		}
		for (auto& ev : changes) {
			std::get<0>(ev)->state_changed(std::get<1>(ev), std::get<2>(ev));
		}
	}
}

bool PollingServer::observe(int fd, PollObserver *observer) {
	auto r = observers.emplace(std::make_pair(fd, observer));
	if (!r.second) return false;
	observed_sockets.push_back({.fd = fd, .events = POLLIN, .revents = 0});
	INFO("observe: now "<<observed_sockets.size()<<" watched (added " << observed_sockets.back().fd << ")");
	return true;
}

bool PollingServer::forget(int fd) {
	size_t r = observers.erase(fd);
	if (!r) return false;
	for (auto it = observed_sockets.begin(); it != observed_sockets.end(); ++it) {
		if (it->fd == fd) {
			observed_sockets.erase(it);
			break;
		}
	}
	INFO("observe: now "<<observed_sockets.size()<<" watched (removed " << fd << ")");
	return true;
}

PollingServer::PollingServer() : observed_sockets(), observers() {

}

void TunerServer::retune(control::channel *ch) {
	if (ch == nullptr && station_socket != nullptr) {
		// Just stop listening
		poll_server.forget(station_socket->fd());
		station_socket.reset(nullptr);
	}
	else if (station_socket != nullptr && station_socket->channel == *ch) {
		// We can reuse socket
	}
	else {
		if (station_socket != nullptr)
			poll_server.forget(station_socket->fd());
		try {
			
			auto n = std::make_unique<channel_socket>(*ch);
			INFO("Created station socket " << n->fd());
			station_socket.swap(n);
			INFO("Using station socket " << station_socket->fd());
			
			poll_server.observe(station_socket->fd(), this);
		} catch (socket_error) {
			// connecting to station failed
			// (probably a non-multicast address given)
		}
		
	}
	got_first = false;
	buffer.clear();
	retry.clear();
}

void TunerServer::state_changed(int fd, short revents) {
	if (!revents & POLLIN) return;
	if (fd == discover_socket.fd()) {
		// Got discovery packet
		auto data = discover_socket.receive();
		auto type = control::identify(data.data);
		if (type != control::command_type::REPLY) return;
		auto ch = control::reply_what(data);
		// See if we know the station
		for (auto& kch : tuner.known_channels) {
			if (kch->recv_addr == ch.recv_addr && kch->name == ch.name) {
				// Yes!
				INFO("Revisited channel: " << ch.name);
				kch->last_seen = ch.last_seen;
				return;
			}
		}
		// Unknown...
		INFO("Found channel: " << ch.name);
		const std::shared_ptr<control::channel> &pch = std::make_shared<control::channel>(ch);
		auto pos = std::upper_bound(tuner.known_channels.begin(), tuner.known_channels.end(),
		                            pch, [](auto a, auto b){return *a < *b;});
		tuner.known_channels.insert(pos, pch);
		
		if ((ch.name == favorite_name && tuner.active_channel->name != favorite_name) ||
				(favorite_name.size() == 0 && tuner.known_channels.size() == 1)) {
			tuner.set_station(pch);
		} else {
			tuner.telnet_server->menu_redraw();
		}
	} else if (fd == station_socket->fd()) {
		// Got data packet
		auto data = station_socket->receive();
		if (data.size < audio::PACKET_SIZE) return;
		audio::packet* pkt = (audio::packet *) data.data.c_str();
		if (!got_first) {
			session_id = pkt->session_id();
		}
		if (pkt->session_id() != session_id) return;
		auto missing = buffer.insert_data(*pkt, data.size - audio::PACKET_SIZE);
		retry.update(missing);
	}
}

radio_tuner &TunerServer::get_tuner() {
	return tuner;
}

TunerServer::TunerServer(PollingServer& poll, std::string favorite_name, broadcast_sender_reply_socket& discover_socket, TelnetServer& telnet, player_buffer& buf)
		: station_socket(), discover_socket(discover_socket),
		  poll_server(poll), tuner(this, &telnet), buffer(buf), favorite_name(favorite_name) {
	poll.observe(discover_socket.fd(), this);
}

void TunerServer::send_rexmit(boost::posix_time::ptime now) {
	auto missing = retry.retry_str(now);
	if (missing.size() > 0) {
		INFO("Trying REXMIT");
		auto rexmit = control::REXMIT_STR;
		rexmit.reserve(missing.size() + 2);
		rexmit.append(1, ' ');
		rexmit.append(missing);
		rexmit.append(1, '\n');
		station_socket->send(rexmit);
		INFO(rexmit);
	}
}

void TunerServer::cleanup_stations() {
	tuner.cleanup_stations();
}
