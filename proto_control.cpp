#include "proto.h"
#include "socket.h"

namespace control
{
	command_type identify(std::string req) {
		if (is_prefix(LOOKUP_STR, req)) return command_type::LOOKUP;
		if (is_prefix(REXMIT_STR, req)) return command_type::REXMIT;
		if (is_prefix(REPLY_STR, req)) return command_type::REPLY;
		return command_type::INVALID;
	}
	
	bool is_prefix(std::string what, std::string of) {
		if (what.size() < of.size()) {
			return of.compare(0, what.size(), what) == 0;
		}
		return false;
	}
	
	std::set <packid_t> rexmit_what(std::string req) {
		std::set <packid_t> r;
		const char *curr = req.c_str() + REXMIT_STR.size() + 1;
		while (curr < req.c_str() + req.size()) {
			char *last = const_cast<char *>(curr);
			auto num = std::strtoull(curr, &last, 10);
			if (num == ULLONG_MAX && errno == ERANGE) continue; // Number too big
			if (num == 0 && last == curr) break; // Whatever is next is not a number
			if (*last != ',' && *last != '\n') {
				// Error in packet
				r.clear();
				return r;
			}
			r.insert(num);
			if (*last == '\n') break;
			curr = last + 1;
		}
		return r;
	}
	
	channel reply_what(received_data<std::string> req) {
		try {
			const char *curr = req.data.c_str() + REPLY_STR.size();
			const char *reqend = req.data.c_str() + req.data.size();
			if (curr > reqend || *curr != ' ') return {};
			++curr;
			char addr_s[20] = {};
			size_t pos = 0;
			while (pos < 19 && *curr != ' ' && *curr != '\0') {
				addr_s[pos++] = *curr;
				++curr;
			}
			if (*curr != ' ') return {};
			++curr;
			sockaddr_in addr;
			addr.sin_family = AF_INET;
			if (inet_aton(addr_s, &addr.sin_addr) == 0)
				return {};
			
			
			const char *ncurr;
			uint16_t port = strtounum<uint16_t>(curr, &ncurr);
			addr.sin_port = htons(port);
			curr = ncurr;
			if (*curr != ' ') return {};
			++curr;
			ncurr = curr;
			
			std::string name(curr);
			if (name[name.size() - 1] != '\n' || name.size() > 65) return {};
			name.resize(name.size() - 1);
			return channel(addr, req.from, name);
		} catch (...) { return {}; }
	}
	
	std::string make_reply(const std::string& mcast_addr, uint16_t data_port, const std::string& station_name) {
		auto r = REPLY_STR + " " + mcast_addr + " " + std::to_string(data_port) + " " + station_name + "\n";
		return r;
	}
	
	std::string make_lookup() {
		static const auto str = LOOKUP_STR + "\n";
		return str;
	}
	
	bool operator<(const channel &a, const channel &b) {
		return a.name < b.name;
	}
	
	bool operator==(const channel &a, const channel &b) {
		return a.server_addr == b.server_addr && a.recv_addr == b.recv_addr && a.name == b.name;
	}
}

bool operator==(const sockaddr_in &a, const sockaddr_in &b) {
	return a.sin_addr.s_addr == b.sin_addr.s_addr && a.sin_family == b.sin_family && a.sin_port == b.sin_port;
}
