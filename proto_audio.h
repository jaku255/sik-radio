#ifndef RADIO_PROTO_AUDIO_H
#define RADIO_PROTO_AUDIO_H

#include <cstdint>
#include "proto.h"

namespace audio {
	class packet {
	public:
		std::uint64_t _session_id;
		packid_t _first_byte_num;
		
		/// Array of PSIZE size containing subsequent data bytes
		char audio_data[1];
		
		/// Constant during broadcast; UNIX time when server started
		std::uint64_t session_id() const {return boost::endian::big_to_native(_session_id);}
		void session_id(std::uint64_t val) {_session_id = boost::endian::native_to_big(val);}
		/// Number of first byte in audio_data
		std::uint64_t first_byte_num() const {return boost::endian::big_to_native(_first_byte_num);}
		void first_byte_num(std::uint64_t val) {_first_byte_num = boost::endian::native_to_big(val);}
		void import_data(const char* data, size_t size, packid_t new_first_byte_num) {
			first_byte_num(new_first_byte_num);
			memcpy(audio_data, data, size);
		}
	};
	
	static const size_t PACKET_SIZE = sizeof(std::uint64_t) + sizeof(packid_t);
}

#endif //RADIO_PROTO_AUDIO_H
