#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ctime>
#include <cerrno>
#include <cstdint>
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "proto.h"
#include "util.h"
#include "telnet.h"
#include "recv_data.h"

using namespace std;
namespace po = boost::program_options;

std::string DISCOVER_ADDR;	///< adres używany przez odbiornik do wykrywania aktywnych nadajników, ustawiany parametrem -d odbiornika, domyślnie 255.255.255.255

uint16_t CTRL_PORT; 		///< port UDP używany do transmisji pakietów kontrolnych, ustawiany parametrem -C nadajnika i odbiornika, domyślnie 30000 + (numer_albumu % 10000)
uint16_t UI_PORT;           ///< port TCP, na którym udostępniany jest prosty interfejs tekstowy do przełączania się między stacjami, domyślnie 10000 + (numer_albumu % 10000); ustawiany parametrem -U odbiornika
uint64_t BSIZE;             ///< rozmiar w bajtach bufora, ustawiany parametrem -b odbiornika, domyślnie 64kB (65536B)
uint16_t RTIME; 			///< czas (w milisekundach) pomiędzy wysłaniem kolejnych raportów o brakujących paczkach (dla odbiorników) oraz czas pomiędzy kolejnymi retransmisjami paczek, ustawiany parametrem -R, domyślnie 250.
std::string NAZWA;

int main(int argc, char **argv) {
	po::options_description desc("Allowed options");
	desc.add_options()
			("discover-addr,d",po::value(&DISCOVER_ADDR)->default_value("255.255.255.255"))
			("control-port,C", po::value(&CTRL_PORT)->default_value(30000 + (385084 % 10000)))
			("ui-port,U", po::value(&UI_PORT)->default_value(10000 + (385084 % 10000)))
			("bsize,b", po::value(&BSIZE)->default_value(65536U))
			("rtime,R", po::value(&RTIME)->default_value(250))
			("nazwa,n", po::value(&NAZWA)->default_value(""));
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
	try {
		// otworzenie gniazd
		broadcast_sender_reply_socket lookup_socket(DISCOVER_ADDR, CTRL_PORT);
		
		
		PollingServer dispatcher;
		dispatcher.set_timeout(0);
		TelnetServer telnet(dispatcher, UI_PORT);
		player_buffer buffer(BSIZE);
		
		TunerServer tuner(dispatcher, NAZWA, lookup_socket, telnet, buffer);
		telnet.set_tuner(&tuner.get_tuner());
		
		
		auto timer = boost::posix_time::microsec_clock::universal_time() - boost::posix_time::seconds(10);
		
		while (true) {
			// During one tick:
			//      LOOKUP is sent if timer was set more than 5secs ago
			//      input is received and possibly played
			//      REPLY packets are processed
			
			// Send LOOKUP
			auto now = boost::posix_time::microsec_clock::universal_time();
			boost::posix_time::time_duration diff = now - timer;
			if (diff > boost::posix_time::seconds(5)) {
				INFO("Sending LOOKUP");
				lookup_socket.send(control::make_lookup());
				timer = now;
			}
			
			// Send REXMIT
			tuner.send_rexmit(now);
			
			// Cleanup old stations
			tuner.cleanup_stations();
			
			
			dispatcher.tick();
			if (buffer.has_byte()) {
				try {
					buffer.output();
				} catch (underflow_error& e) {
					tuner.get_tuner().set_station(tuner.get_tuner().active_channel);
				}
			}
		}
	} catch (useful_error& e) {
		std::cerr << e.text() << std::endl;
	}
}