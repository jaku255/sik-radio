#include "recv_data.h"
#include <utility>
#include "telnet.h"

void radio_tuner::prev() {
	if (known_channels.size() < 1) return;
	if (active_channel == known_channels.back()) return;
	if (active_channel == nullptr) set_station(known_channels.back());
	else
		for (size_t i = 0; i < known_channels.size(); ++i) {
			if (active_channel == known_channels[i]) {
				set_station(known_channels[i-1]);
				break;
			}
		}
		
	if (active_channel == nullptr) set_station(known_channels.back());
}

void radio_tuner::next() {
	if (known_channels.size() < 1) return;
	if (active_channel == known_channels.back()) return;
	if (active_channel == nullptr) set_station(known_channels.front());
	else
		for (size_t i = 0; i < known_channels.size(); ++i) {
			if (active_channel == known_channels[i]) {
				set_station(known_channels[i+1]);
				break;
			}
		}
		
	if (active_channel == nullptr) set_station(known_channels.front());
}

void radio_tuner::set_station(std::shared_ptr<control::channel> ch) {
	INFO("Changing station: " << (ch==nullptr?"<NONE>":ch->name));
	active_channel = std::move(ch);
	telnet_server->menu_redraw();
	tuner_server->retune(active_channel.get());
}

void radio_tuner::cleanup_stations() {
	auto cutoff = boost::posix_time::microsec_clock::universal_time() - boost::posix_time::seconds(20);
	bool reset = false;
	bool changed = false;
	for (auto it = known_channels.begin(); it != known_channels.end();) {
		if ((*it)->last_seen < cutoff) {
			if (active_channel == *it) {
				reset = true;
			}
			it = known_channels.erase(it);
			changed = true;
		} else ++it;
	}
	if (reset) {
		if (known_channels.empty()) set_station(nullptr);
		else set_station(known_channels.front());
	}
	if (changed && !reset) {
		telnet_server->menu_redraw();
	}
}
