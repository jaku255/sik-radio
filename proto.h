#ifndef RADIO_PROTO_H
#define RADIO_PROTO_H

#include <boost/endian/conversion.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <cstdint>
#include <string>
#include <set>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using packid_t = std::uint64_t;

#include "util.h"

template<typename T>
struct received_data {
	sockaddr_in from; ///< Dota of packet sender's
	size_t size;      ///< Size of received data
	T data;           ///< Received data
};

bool operator==(const sockaddr_in& a, const sockaddr_in& b);

namespace control {
	/// Represents a known radio station
	struct channel {
		sockaddr_in recv_addr;
		sockaddr_in server_addr;
		std::string name;
		boost::posix_time::ptime last_seen;
		channel(sockaddr_in addr, sockaddr_in server, std::string& name) : recv_addr(addr), server_addr(server), name(name),
		                                               last_seen(boost::posix_time::microsec_clock::universal_time()) {}
		channel() = default;
	};
	bool operator<(const channel& a, const channel& b);
	bool operator==(const channel& a, const channel& b);
	
	static std::string LOOKUP_STR = "ZERO_SEVEN_COME_IN";
	static std::string REPLY_STR = "BOREWICZ_HERE";
	static std::string REXMIT_STR = "LOUDER_PLEASE";
	
	enum class command_type {
		INVALID,
		LOOKUP,
		REXMIT,
		REPLY
	};
	
	bool is_prefix(std::string what, std::string of);
	
	command_type identify(std::string req);
	
	std::set<packid_t> rexmit_what(std::string req);
	
	channel reply_what(received_data<std::string> req);
	
	std::string make_reply(const std::string& mcast_addr, uint16_t data_port, const std::string& station_name);
	
	std::string make_lookup();
}

#endif //RADIO_PROTO_H
