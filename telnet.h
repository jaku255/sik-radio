#ifndef RADIO_TELNET_H
#define RADIO_TELNET_H

#include <limits.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>
#include <functional>
#include <iostream>
#include <map>
#include <sstream>
#include "proto.h"
#include "socket.h"
#include "recv_data.h"

#define BUF_SIZE 1024

namespace TN {
	typedef unsigned char option;
	
	const option NUL = 0;
	const option LF = 10;
	const option CR = 13;
	const option BEL = 7;
	
	const option IAC = 255;
	const option SE = 240; ///< End of subnegotiation parameters.
	const option NOP = 241; ///< No operation.
	const option DM = 242; ///< The data stream portion of a Synch. This should always be accompanied by a TCP Urgent notification.
	const option BRK = 243; ///< NVT character BRK.
	const option IP = 244; ///< Interrupt Process
	const option AO = 245; ///< Abort Output
	const option AYT = 246; ///< Are You There
	const option EC = 247; ///< Erase character
	const option EL = 248; ///< Erase line
	const option GA = 249; ///< Go ahead
	const option SB = 250; ///< Indicates that what follows is subnegotiation of the indicated option.
	const option WILL = 251;
	const option WONT = 252;
	const option DO = 253;
	const option DONT = 254;
	
	const option LINEMODE = 34;
	const option SUPPRESS_GO_AHEAD = 3;
	const option ECHO = 1;
	
	const option LM_MODE = 1;
	const option LM_FORWARDMASK = 2;
	const option LM_SLC = 3;
	const option LM_MODE_ACK = 4;
}

namespace ANSI {
	typedef unsigned char byte;
	const byte ESC = 27;
	const byte CSI = '[';
	const byte CLS[] = {ESC, CSI, '2', 'J', ESC, CSI, '3', 'J', ESC, CSI, '1', ';', '1', 'H', 0};
}

using namespace TN;

enum DisconnectReason {
	Broken = 0, InvalidData, NormalExit, ClientExit
};

static const char* DisconnectReason_str[4] = {"Connection broken",
							  "Unrecoverable data error",
							  "Normal exit",
							  "Client exit"};

class client_disconnected : public std::exception {
	DisconnectReason _what;
	
public:
	explicit client_disconnected(DisconnectReason what) noexcept {
		_what = what;
	}
	
	const char *what() const noexcept override {
		return DisconnectReason_str[_what];
	}
	
	DisconnectReason reason() const noexcept {
		return _what;
	}
};

class SocketBuf : public std::stringbuf {
	int fd;
public:
	explicit SocketBuf(int fd) : fd(fd) {}
	virtual int sync() {
		std::string buf = this->str();
		ssize_t r = write(fd, buf.data(), buf.size());
		if (r < 0) {
			throw client_disconnected(Broken);
		} else if (static_cast<unsigned long>(r) == buf.size())
			this->str("");
		else {
			this->str(buf.substr(static_cast<unsigned long>(r), std::string::npos));
		}
		return 0;
	}
};

class TelnetNegotiator {
	bool iac = false;
	bool sb = false;
	option cmd = NUL;
	std::vector<option> history;
	std::ostream& output;
	
	void reset();

public:
	TelnetNegotiator(std::ostream& output) : output(output) {}
	void init();
	
	inline bool process(option opt);
};

class radio_tuner;

static const std::string Separator(72, '-');
static const std::string Title("SIK Radio");

struct ClientStatus {
	std::vector<unsigned char> seq;
	SocketBuf output_buf;
	std::ostream output;
	TelnetNegotiator filter;
	radio_tuner* tuner;
	int fd;
	
	explicit ClientStatus(int fd, radio_tuner* tuner) : seq(), output_buf(fd), output(&output_buf), filter(output),
	                                                    tuner(tuner), fd(fd) {
		filter.init();
	}
	void print();
	void input(unsigned char b);
	
};

class PollObserver {
public:
	virtual ~PollObserver() = default;
	virtual void state_changed(int fd, short revents) = 0;
};

class PollingServer {
	std::vector<pollfd> observed_sockets;
	std::map<int, PollObserver*> observers;
	int timeout; ///< Timeout in ms
public:
	PollingServer();
	bool observe(int fd, PollObserver* observer);
	bool forget(int fd);
	void set_timeout(int ms) { timeout = ms; }
	void tick();
};

class TelnetServer : public PollObserver {
	std::map<int, ClientStatus> clients;
	int server_fd;
	char buf[BUF_SIZE];
	PollingServer& poll_server;
	radio_tuner* tuner;

public:
	TelnetServer(PollingServer& pserver, uint16_t port);
	void state_changed(int fd, short revents) override;
	void menu_redraw();
	void set_tuner(radio_tuner* tun);
	~TelnetServer() override;
	
	struct sockaddr_in server;
};

class TunerServer : public PollObserver {
	std::unique_ptr<channel_socket> station_socket;
	broadcast_sender_reply_socket& discover_socket;
	PollingServer& poll_server;
	radio_tuner tuner;
	player_buffer& buffer;
	
	std::string favorite_name;
	
	// Stan serwera
	bool got_first = false;
	std::uint64_t session_id = 0;
	
	friend class radio_tuner;
	void retune(control::channel* ch);
public:
	retry_set retry;
	TunerServer(PollingServer& poll, std::string favorite_name, broadcast_sender_reply_socket& discover_socket , TelnetServer& telnet, player_buffer&);
	void state_changed(int fd, short revents) override;
	
	void cleanup_stations();
	radio_tuner& get_tuner();
	void send_rexmit(boost::posix_time::ptime now);
};

#endif //RADIO_TELNET_H
