#ifndef RADIO_RECV_DATA_H
#define RADIO_RECV_DATA_H

#include <netinet/in.h>
#include <cstdint>
#include <string>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include "proto.h"

class TelnetServer;
class TunerServer;

class radio_tuner {
public:
	std::vector<std::shared_ptr<control::channel>> known_channels;
	std::shared_ptr<control::channel> active_channel;
	TelnetServer* telnet_server;
	TunerServer* tuner_server;
	explicit radio_tuner(TunerServer* tsrv, TelnetServer* srv) : known_channels(), active_channel(), telnet_server(srv), tuner_server(tsrv) {}
	void prev();
	void next();
	void set_station(std::shared_ptr<control::channel> ch);
	void cleanup_stations();
};

extern radio_tuner* TUNER;

#endif //RADIO_RECV_DATA_H
