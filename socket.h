//
// Created by jaku255 on 08.06.2018.
//

#ifndef RADIO_SOCKET_H
#define RADIO_SOCKET_H

#include <string>
#include "util.h"
#include "proto_audio.h"
#include "proto.h"

class socket_error : public useful_error {
	const char* operation;
	const int err;
public:
	explicit socket_error(const char* operation) noexcept : operation(operation), err(errno) {}
	const char *what() const noexcept override {
		return "Error during socket operation";
	}
	
	const std::string text() override {
		return std::string("Error during socket operation: ") + operation + strerror(err);
	}
};



class base_socket {
protected:
	int _socket = -1; ///< File descriptor of given socket
	base_socket() {}
public:
	virtual ~base_socket() {
		close(_socket);
	}
	/// File descriptor for use in select()
	/// @warning Do not read(), write() or close() it
	int fd() {
		return _socket;
	}
};

/// Socket that allows directed sending of packets
class unbound_sender_socket : public virtual base_socket {
protected:
	unbound_sender_socket() {}
public:
	/// Send msg to given address
	/// @param msg [in] Message to send (automatic '\0' byte at the end will be ignored)
	ssize_t send(std::string msg, sockaddr_in to) {
		auto r = sendto(_socket, msg.c_str(), msg.size(), 0, (sockaddr*)&to, sizeof(to));
		if (r < 0) throw socket_error("sendto");
		return r;
	}
};

/// Socket that has a default destination of packets
class bound_sender_socket : public unbound_sender_socket {
protected:
	/// Default destination of socket
	/// @note Bound socket uses sendto internally, because it allows for unfiltered receive of broadcast reply
	sockaddr_in default_dest;
	bound_sender_socket() {}
public:
	
	sockaddr_in default_destination() { return default_dest; }
	
	/// Send msg to default receiver
	/// @param msg [in] Message to send (automatic '\0' byte at the end will be ignored)
	ssize_t send(std::string msg) {
		return unbound_sender_socket::send(msg, default_dest);
	}
	
	/// Send audio packet of given size to default receiver for this socket
	ssize_t send(audio::packet* val, size_t size) {
		auto r = sendto(_socket, val, size+audio::PACKET_SIZE, 0, (sockaddr*)&default_dest, sizeof(default_dest));
		if (r < 0) throw socket_error("write");
		return r;
	}
};

/// Socket that can receive data
class receiver_socket : public virtual base_socket {
protected:
	receiver_socket() {}
public:
	/// Receive entire packet of unknown size
	received_data<std::string> receive() {
		auto recvlen = recv(_socket, nullptr, 0, MSG_PEEK|MSG_TRUNC);
		if (recvlen < 0) throw socket_error("recv");
		if (recvlen == 0) return {};
		char *buf = new char[recvlen]; // Buffer to save data to
		
		sockaddr_in other_addr;
		socklen_t addrlen = sizeof(other_addr);
		
		if ((recvlen = recvfrom(_socket, (char*)buf, (size_t)recvlen, 0, (sockaddr*)&other_addr, &addrlen)) < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) return {};
			throw socket_error("recvfrom");
		}
		
		std::string r(buf, (size_t)recvlen);
		delete[] buf;
		received_data<std::string> ret;
		ret.from = other_addr;
		ret.size = (size_t)recvlen;
		ret.data = std::move(r);
		return ret;
	}
};

/// Socket that by default sends to dotted_address, which may be broadcast address,
/// and can receive replies
class broadcast_sender_reply_socket : public bound_sender_socket, public receiver_socket {
public:
	/// @param port default port to send to
	broadcast_sender_reply_socket(std::string dotted_address, uint16_t port) {
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socket < 0)
			throw socket_error("Cannot initialize socket");
		
		int enable = 1;
		if (setsockopt(_socket, SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable)) < 0)
			throw socket_error("Cannot enable broadcast");
		
		struct sockaddr_in local_addr;
		local_addr.sin_family = AF_INET;
		local_addr.sin_port = htons(0);
		local_addr.sin_addr.s_addr = htons(INADDR_ANY);
		if (bind(_socket, (struct sockaddr *)&local_addr, sizeof(local_addr)) < 0)
			throw socket_error("Cannot bind to socket");
		
		// default_dest is inherited from bound_sender_socket
		
		default_dest.sin_family = AF_INET;
		default_dest.sin_port = htons(port);
		if (inet_aton(dotted_address.c_str(), &default_dest.sin_addr) == 0)
			throw socket_error("Cannot convert address");
	}
};

/// Socket that can receive broadcasts on given port
class broadcast_recv_socket : public receiver_socket, public unbound_sender_socket {
public:
	explicit broadcast_recv_socket(uint16_t port) {
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socket < 0)
			throw socket_error("Cannot initialize socket");
		
		sockaddr_in addr;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		addr.sin_addr.s_addr = htonl(INADDR_ANY);
		
		int enable = 1;
		if (setsockopt(_socket, SOL_SOCKET, SO_BROADCAST, &enable, sizeof(enable)) < 0)
			throw socket_error("Cannot enable broadcast");
		
		if (setsockopt(_socket, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(enable)) < 0)
			throw socket_error("Cannot reuse port");
		
		if (bind(_socket, (struct sockaddr *)&addr, sizeof addr) < 0)
			throw socket_error("Cannot bind to socket");
	}
};

/// Socket that can receive multicast and send control packets to server
class channel_socket : public receiver_socket, public bound_sender_socket {
public:
	control::channel& channel;
	channel_socket(control::channel& ch) : channel(ch) {
		_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
		if (_socket < 0)
			throw socket_error("socket");
		
		sockaddr_in addr = ch.recv_addr;
		if (bind(_socket, (sockaddr*)&addr, sizeof(addr)) < 0) {
			throw socket_error("bind");
		}
		
		ip_mreq mreq;
		mreq.imr_multiaddr.s_addr = ch.recv_addr.sin_addr.s_addr;
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
			throw socket_error("setsockopt IP_ADD_MEMBERSHIP (is this a valid multicast address?)");
		}
		// note: membership dropped automatically by kernel on close()
		
		default_dest = ch.server_addr;
	}
};

#endif //RADIO_SOCKET_H
