LDFLAGS = -lboost_program_options -lboost_chrono
CXXFLAGS = -Wall -O2 -std=c++17

all: sikradio-receiver sikradio-sender

%.o: %.cpp
	$(CXX) -c -o $@ $^ $(CXXFLAGS)

sikradio-sender: nadajnik.o proto_control.o
	$(CXX) $(LDFLAGS) -o $@ $^

sikradio-receiver: odbiornik.o proto_control.o recv_data.o telnet.o
	$(CXX) $(LDFLAGS) -o $@ $^

clean:
	rm -f *.o sikradio-receiver sikradio-sender

.PHONY:
	clean all