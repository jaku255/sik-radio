#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <ctime>
#include <cerrno>
#include <cstdint>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <poll.h>
#include <csignal> // or C++ style alternative

#include "proto.h"
#include "socket.h"
#include "util.h"

using namespace std;
namespace po = boost::program_options;

std::string MCAST_ADDR; 	///< adres rozgłaszania ukierunkowanego, ustawiany obowiązkowym parametrem -a nadajnika
uint16_t DATA_PORT;		///< port UDP używany do przesyłania danych, ustawiany parametrem -P nadajnika i odbiornika, domyślnie 20000 + (numer_albumu % 10000)
uint16_t CTRL_PORT; 	///< port UDP używany do transmisji pakietów kontrolnych, ustawiany parametrem -C nadajnika i odbiornika, domyślnie 30000 + (numer_albumu % 10000)
uint64_t PSIZE; 		///< rozmiar w bajtach paczki, ustawiany parametrem -p nadajnika, domyślnie 512B
uint64_t FSIZE; 		///< rozmiar w bajtach kolejki FIFO nadajnika, ustawiany parametrem -f nadajnika, domyślnie 128kB.
uint16_t RTIME; 		///< czas (w milisekundach) pomiędzy wysłaniem kolejnych raportów o brakujących paczkach (dla odbiorników) oraz czas pomiędzy kolejnymi retransmisjami paczek, ustawiany parametrem -R, domyślnie 250.
std::string NAZWA; 		///< nazwa to nazwa nadajnika, ustawiana parametrem -n, domyślnie "Nienazwany Nadajnik"


int main(int argc, char **argv) {
	po::options_description desc("Allowed options");
	desc.add_options()
			("mcast-addr,a",po::value(&MCAST_ADDR)->required())
			("data-port,P", po::value(&DATA_PORT)->default_value(20000 + (385084 % 10000)))
			("control-port,C", po::value(&CTRL_PORT)->default_value(30000 + (385084 % 10000)))
			("psize,p", po::value(&PSIZE)->default_value(512U))
			("fsize,f", po::value(&FSIZE)->default_value(131072U))
			("rtime,R", po::value(&RTIME)->default_value(250))
			("nazwa,n", po::value(&NAZWA)->default_value("Nienazwany Nadajnik"));
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);
/*
	Nadajnik otrzymyuje na standardowe wejście strumień danych i wysyłać te dane zapakowane
        w datagramy UDP na port DATA_PORT na adres ukierunkowanego rozgłaszania MCAST_ADDR.
    Dane powinny być przesyłane w paczkach po PSIZE bajtów, zgodnie z protokołem opisanym poniżej.
	Nadajnik powinien przechowywać w kolejce FIFO ostatnich FSIZE bajtów przeczytanych z wejścia tak,
		żeby mógł ponownie wysłać te paczki, o których retransmisję poprosiły odbiorniki.
	Nadajnik cały czas zbiera od odbiorników prośby o retransmisje paczek.
		Gromadzi je przez czas RTIME, następnie wysyła serię retransmisji
		(podczas ich wysyłania nadal zbiera prośby, do wysłania w kolejnej serii),
		następnie znów gromadzi je przez czas RTIME, itd.
	Nadajnik nasłuchuje na UDP na porcie CTRL_PORT, przyjmując także pakiety rozgłoszeniowe.
	Powinien rozpoznawać dwa rodzaje komunikatów:
		LOOKUP (prośby o identyfikację): na takie natychmiast odpowiada komunikatem REPLY zgodnie ze specyfikacją protokołu poniżej.
		REXMIT (prośby o retransmisję paczek): na takie nie odpowiada bezpośrednio; raz na jakiś czas ponownie wysyła paczki, według opisu powyżej.
	Po wysłaniu całej zawartości standardowego wejścia nadajnik się kończy z kodem wyjścia 0.
	Jeśli rozmiar odczytanych danych nie jest podzielny przez PSIZE, ostatnia (niekompletna) paczka jest porzucana; nie jest wysyłana.

 Dwa sockety: MCAST_ADDR:DATA_PORT - rozsyłanie
    *:CTRL_PORT - odbieranie poleceń
*/
	//raise(SIGSTOP);
	try {
		// otworzenie gniazd
		broadcast_sender_reply_socket caster(MCAST_ADDR, DATA_PORT);
		broadcast_recv_socket control(CTRL_PORT);
		
		// Stan serwera
		buffer packrat(PSIZE, FSIZE);
		std::set<packid_t> rex_reqs;
		auto timer = boost::posix_time::microsec_clock::universal_time();
		
		audio::packet *packet = (audio::packet *) (malloc(audio::PACKET_SIZE + PSIZE));
		packet->session_id(static_cast<uint64_t>(time(nullptr)));
		std::vector<pollfd> sockets;
		sockets.push_back({.fd = 0, .events = POLLIN, .revents = 0});
		sockets.push_back({.fd = control.fd(), .events = POLLIN, .revents = 0});
		
		
		while (true) {
			// During one tick:
			//      packets are retransmited if RTIME ms have passed since last retransmit
			//      up to PSIZE bytes are read over up to RTIME ms and sent if complete
			//      simultaneously,
			
			// Retransmit if necessary
			if (!rex_reqs.empty()) {
				auto now = boost::posix_time::microsec_clock::universal_time();
				boost::posix_time::time_duration diff = now - timer;
				if (diff > boost::posix_time::milliseconds(RTIME)) {
					INFO("Retransmitting data");
					for (auto pack : rex_reqs) {
						auto data = packrat.packet(pack);
						if (data != nullptr) {
							packet->import_data(data, PSIZE, pack);
							caster.send(packet, PSIZE);
						}
					}
					rex_reqs.clear();
					timer = now;
				}
			}
			
			int ret = poll(sockets.data(), sockets.size(), RTIME);
			
			if (ret == -1) {
				throw run_error("select(stdin)");
			} else if (ret) {
				if (sockets[0].revents & POLLIN) {
					// Got input to send
					//INFO("Loading data");
					packrat.load();
					
					if (packrat.has_next()) {
						auto next_id = packrat.next_id();
						auto p = packrat.packet();
						//if ((next_id / PSIZE) % 500) {
							//INFO("Sending data " << next_id);
							packet->import_data(std::move(p), PSIZE, next_id);
							caster.send(packet, PSIZE);
						//}
					}
				}
				if (sockets[1].revents & POLLIN) {
					auto cmdstr = control.receive();
					auto cmd = control::identify(cmdstr.data);
					switch (cmd) {
						case control::command_type::LOOKUP: {
							INFO("control: LOOKUP");
							auto reply = control::make_reply(MCAST_ADDR, DATA_PORT, NAZWA);
							control.send(reply, cmdstr.from);
						}
							break;
						case control::command_type::REXMIT: {
							auto reqs = control::rexmit_what(cmdstr.data);
							INFO("control: REXMIT " << reqs.size());
							rex_reqs.insert(reqs.begin(), reqs.end());
						}
							break;
						default:
							INFO("control: INVALID");
							break;
					}
				}
			}
		}
	} catch (useful_error& e) {
		std::cerr << e.text() << std::endl;
	}
}